# Releasefeed

Generates Atom feeds for releases on streaming platforms.

### License

Released under the terms of the GNU AGPLv3. See the [`LICENSE`](./LICENSE) file for details.
