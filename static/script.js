document.addEventListener("DOMContentLoaded", e => {
    const input = document.getElementById("feed-url-input")
    input.addEventListener("input", e => handleInput(e.target.value))

    handleInput(input.value)

    document.getElementById("feed-url-copy").addEventListener("click", e => {
        navigator.clipboard.writeText(e.target.dataset.feedUrl)
    })
})

function handleInput(value) {
    document.getElementById("output-container").hidden = value === ""
    if (value === "") {
        return
    }

    const output = document.getElementById("feed-url-output")
    const outputStatus = document.getElementById("feed-url-status")
    const copyButton = document.getElementById("feed-url-copy")

    const url = URL.parse(value)
    const result = {
        error: null,
        providerName: null,
        feed: null,
    }

    if (url === null) {
        result.error = "Not a valid URL"
    } else if (url.host.endsWith("spotify.com")) {
        handleSpotify(url, result)
    } else if (url.host.endsWith("bandcamp.com")) {
        handleBandcamp(url, result)
    } else {
        result.error = "Not a supported provider"
    }

    const valid = result.error === null
    copyButton.hidden = !valid
    output.hidden = !valid
    outputStatus.className = valid ? "valid" : "error"
    if (valid) {
        copyButton.dataset.feedUrl = result.feed.feedUrl
        output.innerHTML = result.feed.display
        outputStatus.innerText = result.providerName
    } else {
        delete copyButton.dataset.feedUrl
        output.innerHTML = null
        outputStatus.innerText = result.error
    }
}

function handleSpotify(url, result) {
    if (!(url.host == "open.spotify.com" || url.host == "play.spotify.com")) {
        result.error = "Unknown Spotify domain"
        return
    }

    const match = url.pathname.match(/\/artist\/([a-z0-9_-]+)/i)
    if (match !== null) {
        result.providerName = "Spotify"
        result.feed = buildUrl("spotify", match[1])
    } else {
        result.error = "Not a Spotify artist URL"
    }
}

function handleBandcamp(url, result) {
    const match = url.host.match(/^([a-z0-9\-]+)\.bandcamp\.com$/i)
    if (match !== null) {
        result.providerName = "Bandcamp"
        result.feed = buildUrl("bandcamp", match[1])
    } else {
        result.error = "Not a valid Bandcamp page"
    }
}

function buildUrl(provider, id) {
    const baseUrl = URL.parse(document.URL)
    const display = `${baseUrl.origin}/releases/<b>${provider}</b>/<b>${id}</b>`
    baseUrl.pathname = `/releases/${provider}/${id}`
    return { feedUrl: baseUrl.href, display }
}
