use askama::Template;

use crate::entities::{Artist, FeedData, Release};

#[derive(Template)]
#[template(path = "feed.xml")]
pub(crate) struct Feed<'a> {
    pub(crate) data: &'a FeedData,
}

#[derive(Template)]
#[template(path = "entry.html")]
pub(crate) struct FeedEntry<'a> {
    release: &'a Release,
    artist_credit: ArtistCredit<'a>,
}

impl<'a> FeedEntry<'a> {
    pub(crate) fn new(release: &'a Release) -> Self {
        FeedEntry {
            release,
            artist_credit: ArtistCredit::new(&release.artists),
        }
    }
}

#[derive(Template)]
#[template(path = "artist_credit.html")]
struct ArtistCredit<'a> {
    head: &'a Artist,
    tail: &'a [Artist],
}

impl<'a> ArtistCredit<'a> {
    fn new(artists: &'a [Artist]) -> ArtistCredit<'a> {
        assert!(!artists.is_empty());

        ArtistCredit {
            head: &artists[0],
            tail: &artists[1..],
        }
    }
}
