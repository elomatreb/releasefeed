pub(crate) mod bandcamp;
pub(crate) mod spotify;

use std::{
    fmt,
    sync::Arc,
    time::{Duration, SystemTime},
};

use anyhow::{Context, Result};
use askama::Template;
use axum::{
    extract::Path,
    http::{header, StatusCode},
    response::{IntoResponse, Response},
    Extension,
};
use axum_extra::{
    headers::{Expires, IfModifiedSince, LastModified},
    TypedHeader,
};
use deadpool_redis::redis::AsyncCommands;
use serde::Deserialize;
use tracing::{debug, error, instrument, trace};

use self::{bandcamp::BandcampArtistId, spotify::SpotifyId};
use crate::{
    entities::{ArtistId, CacheEntry, FeedData},
    templates, Configuration, State,
};

pub(crate) fn start() -> Providers {
    debug!("starting provider actors");
    let spotify = spotify::SpotifyActor::start();
    let bandcamp = bandcamp::BandcampActor::start();

    Providers { spotify, bandcamp }
}

pub(crate) struct Providers {
    pub(crate) spotify: spotify::SpotifyActor,
    pub(crate) bandcamp: bandcamp::BandcampActor,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub(crate) enum Provider {
    Spotify,
    Bandcamp,
}

impl Provider {
    pub(crate) fn id(&self) -> &'static str {
        match self {
            Provider::Spotify => "spotify",
            Provider::Bandcamp => "bandcamp",
        }
    }
}

impl fmt::Display for Provider {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Provider::Spotify => write!(f, "Spotify"),
            Provider::Bandcamp => write!(f, "Bandcamp"),
        }
    }
}

pub(super) async fn get(
    Extension(state): Extension<Arc<State>>,
    Path((provider, id)): Path<(Provider, String)>,
    modified_since: Option<TypedHeader<IfModifiedSince>>,
) -> Result<Response, RequestError> {
    debug!(?provider, id);
    let id = artist_id(provider, id)?;

    let feed = match get_cached(&state, &id).await? {
        Some(CacheEntry::Success(f)) => {
            debug!("found cached feed");
            f
        }
        Some(CacheEntry::NotFound) => {
            debug!("found cached 404");
            return Err(RequestError::NotFound);
        }
        None => {
            debug!("requesting uncached feed");
            get_feed(&state, id).await?
        }
    };

    let last_modified = SystemTime::from(feed.retrieved_at);
    let expires =
        last_modified + Duration::from_secs(Configuration::get().redis.cache_duration as _);

    if let Some(TypedHeader(modified_since)) = modified_since {
        let modified_since = SystemTime::from(modified_since);

        if modified_since >= last_modified {
            return Ok(StatusCode::NOT_MODIFIED.into_response());
        }
    }

    let rendered = templates::Feed { data: &feed }.render().unwrap();
    Ok((
        [(header::CONTENT_TYPE, "application/atom+xml")],
        TypedHeader(LastModified::from(last_modified)),
        TypedHeader(Expires::from(expires)),
        rendered,
    )
        .into_response())
}

/// Try to load a feed from the cache.
///
/// Returns `Ok(None)` if no cache entry exists.
#[instrument(skip(state))]
async fn get_cached(state: &State, artist: &ArtistId) -> Result<Option<CacheEntry>> {
    let cache_key = artist.cache_key();
    trace!(?cache_key);

    let mut connection = state
        .redis
        .get()
        .await
        .context("Failed to retrieve Redis connection")?;

    trace!("checking cache");
    let cache_entry = connection
        .get::<_, Option<String>>(&cache_key)
        .await
        .context("Failed to retrieve cached object")?;

    if let Some(entry) = cache_entry {
        serde_json::from_str(&entry)
            .map(Some)
            .context("Failed to deserialize cache entry")
    } else {
        debug!("no cached entry");
        Ok(None)
    }
}

/// Get the feed data for the given artist.
///
/// If successful, returns the feed data and a bool that is true if the data was cached.
#[instrument(skip(state))]
async fn get_feed(state: &State, artist: ArtistId) -> Result<FeedData, RequestError> {
    debug!("requesting fresh feed");
    let cache_key = artist.cache_key();

    let result = match artist {
        ArtistId::Spotify(id) => state.providers.spotify.load(id).await,
        ArtistId::Bandcamp(id) => state.providers.bandcamp.load(id).await,
    };

    trace!("finished loading feed");

    match &result {
        Ok(f) => {
            trace!(?cache_key, "caching successfull result");
            insert_into_cache(state, &cache_key, CacheEntry::Success(f.clone())).await;
        }
        Err(RequestError::NotFound) => {
            trace!(?cache_key, "caching 404");
            insert_into_cache(state, &cache_key, CacheEntry::NotFound).await;
        }
        Err(RequestError::BadId | RequestError::Other(_)) => (),
    }

    result
}

fn artist_id(provider: Provider, id: String) -> Result<ArtistId, RequestError> {
    Ok(match provider {
        Provider::Spotify => {
            ArtistId::Spotify(SpotifyId::try_from(id).map_err(|_| RequestError::BadId)?)
        }
        Provider::Bandcamp => {
            ArtistId::Bandcamp(BandcampArtistId::from_slug(&id).map_err(|_| RequestError::BadId)?)
        }
    })
}

/// Insert the given entry into the cache.
async fn insert_into_cache(state: &State, cache_key: &str, entry: CacheEntry) {
    if let Err(error) = try_insert_into_cache(state, cache_key, &entry).await {
        let error = format!("{error:#}");
        error!(%cache_key, ?error, "failed to cache feed");
    } else {
        debug!(%cache_key, "cached feed");
    }
}

async fn try_insert_into_cache(state: &State, cache_key: &str, entry: &CacheEntry) -> Result<()> {
    let cache_duration = Configuration::get().redis.cache_duration;
    let feed = serde_json::to_string(entry).unwrap();

    let mut connection = state
        .redis
        .get()
        .await
        .context("Failed to retrieve pool connection for caching")?;

    connection
        .set_ex::<_, _, ()>(cache_key, feed, cache_duration)
        .await
        .context("Failed to insert cache entry")?;

    Ok(())
}

#[derive(Debug)]
pub(crate) enum RequestError {
    NotFound,
    BadId,
    Other(anyhow::Error),
}

impl IntoResponse for RequestError {
    fn into_response(self) -> Response {
        match self {
            RequestError::NotFound => (StatusCode::NOT_FOUND, "artist not found").into_response(),
            RequestError::BadId => (StatusCode::BAD_REQUEST, "invalid artist ID").into_response(),
            RequestError::Other(e) => {
                let error = format!("{e:#}");
                error!(%error, "request error");
                (StatusCode::INTERNAL_SERVER_ERROR, "internal error").into_response()
            }
        }
    }
}

impl From<anyhow::Error> for RequestError {
    fn from(e: anyhow::Error) -> RequestError {
        RequestError::Other(e)
    }
}
