use std::{
    cmp, fmt,
    sync::LazyLock,
    time::{Duration, Instant},
};

use anyhow::{anyhow, bail, Context, Result};
use reqwest::{Client, StatusCode, Url};
use scraper::{Html, Selector};
use serde::{de::Error, Deserialize, Deserializer, Serialize};
use time::{
    format_description::FormatItem, macros::format_description, OffsetDateTime, PrimitiveDateTime,
};
use tokio::{
    sync::{
        mpsc::{self, Receiver, Sender},
        oneshot,
    },
    time::sleep_until,
};
use tracing::{debug, error, info_span, instrument, trace, Instrument};

use crate::{
    entities::{Artist, ArtistId, FeedData, Release, ReleaseId},
    providers::RequestError,
    Configuration,
};

/// Minimum time between two Bandcamp web requests.
const REQUEST_INTERVAL: Duration = Duration::from_millis(300);

pub(crate) struct BandcampActor {
    tx: Sender<Action>,
}

impl BandcampActor {
    pub(super) fn start() -> BandcampActor {
        let span = info_span!("bandcamp");
        debug!(parent: &span, "starting Bandcamp actor");

        let (tx, rx) = mpsc::channel(4);

        tokio::spawn(BandcampActor::run(rx).instrument(span));

        BandcampActor { tx }
    }

    pub(super) async fn load(&self, artist_id: BandcampArtistId) -> Result<FeedData, RequestError> {
        let (tx, rx) = oneshot::channel();
        self.send_action(Action::GetArtist {
            id: artist_id.clone(),
            responder: tx,
        })
        .await;

        let artist = rx
            .await
            .expect("actor did not respond")?
            .ok_or(RequestError::NotFound)?;

        let (tx, rx) = oneshot::channel();
        self.send_action(Action::GetReleases {
            releases: artist.releases,
            responder: tx,
        })
        .await;

        let releases = rx
            .await
            .expect("actor did not respond")?
            .into_iter()
            .map(Release::from)
            .collect();

        Ok(FeedData {
            artist: Artist {
                name: artist.name,
                id: artist_id.into(),
            },
            releases,
            retrieved_at: OffsetDateTime::now_utc(),
        })
    }

    async fn send_action(&self, action: Action) {
        self.tx.send(action).await.expect("actor is not running");
    }

    async fn run(mut requests: Receiver<Action>) {
        let client = Client::builder().gzip(true).build().unwrap();
        let mut last_request = Instant::now();

        while let Some(action) = requests.recv().await {
            match action {
                Action::GetArtist { id, responder } => {
                    let artist = get_artist(&client, &mut last_request, &id).await;
                    let _ = responder.send(artist);
                }
                Action::GetReleases {
                    releases,
                    responder,
                } => {
                    let releases = get_releases(&client, &mut last_request, releases).await;
                    let _ = responder.send(releases);
                }
            }
        }
    }
}

/// Apply the minimum wait between requests.
async fn wait_between_requests(last_request: &mut Instant) {
    if last_request.elapsed() < REQUEST_INTERVAL {
        trace!(
            wait = ?(REQUEST_INTERVAL - last_request.elapsed()),
            "need to wait"
        );
        sleep_until((*last_request + REQUEST_INTERVAL).into()).await;
    }

    *last_request = Instant::now();
}

#[derive(Clone, Serialize, Deserialize)]
pub(crate) struct BandcampArtistId(String);

impl BandcampArtistId {
    pub(crate) fn from_slug(slug: &str) -> Result<BandcampArtistId, &'static str> {
        if slug.is_empty() {
            return Err("ID cannot be empty");
        }

        if !slug.chars().all(|c| c.is_ascii_alphanumeric() || c == '-') {
            return Err("invalid character in ID");
        }

        Ok(BandcampArtistId(format!("https://{slug}.bandcamp.com")))
    }

    fn from_url(url: &Url) -> BandcampArtistId {
        BandcampArtistId(url.to_string())
    }

    pub(crate) fn url(&self) -> String {
        format!("{}/music", self.0)
    }
}

impl fmt::Debug for BandcampArtistId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for BandcampArtistId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<BandcampArtistId> for ArtistId {
    fn from(id: BandcampArtistId) -> Self {
        ArtistId::Bandcamp(id)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct BandcampReleaseId(Url);

impl fmt::Display for BandcampReleaseId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Debug)]
enum Action {
    GetArtist {
        id: BandcampArtistId,
        responder: oneshot::Sender<Result<Option<BandcampArtist>>>,
    },
    GetReleases {
        releases: Vec<Url>,
        responder: oneshot::Sender<Result<Vec<BandcampRelease>>>,
    },
}

#[derive(Debug)]
struct BandcampArtist {
    name: String,
    releases: Vec<Url>,
}

#[instrument(skip(client, last_request))]
async fn get_artist(
    client: &Client,
    last_request: &mut Instant,
    id: &BandcampArtistId,
) -> Result<Option<BandcampArtist>> {
    let artist_url = Url::parse(&id.url()).unwrap();

    wait_between_requests(last_request).await;
    trace!("sending overview request");
    let resp = client
        .get(artist_url)
        .send()
        .await
        .context("Failed to get artist overview page")?;

    let status_code = resp.status();
    let body = resp
        .text()
        .await
        .context("Failed to get artist overview page")?;
    trace!("completed overview request");

    if status_code == StatusCode::OK {
        let data = scrape_artist_overview(&body, id)?;
        Ok(Some(data))
    } else if status_code == StatusCode::NOT_FOUND {
        debug!("artist does not exist");
        Ok(None)
    } else {
        error!(code = ?status_code, "artist overview returned unexpected status code");
        Err(anyhow!("Unexpected status code"))
    }
}

fn scrape_artist_overview(body: &str, artist_id: &BandcampArtistId) -> Result<BandcampArtist> {
    static NAME_SELECTOR: LazyLock<Selector> =
        LazyLock::new(|| Selector::parse("#band-name-location > .title").unwrap());
    static RELEASES_SELECTOR: LazyLock<Selector> =
        LazyLock::new(|| Selector::parse(".music-grid-item > a").unwrap());

    let document = Html::parse_document(body);

    let name = document
        .select(&NAME_SELECTOR)
        .next()
        .and_then(|e| e.text().next())
        .ok_or_else(|| anyhow!("Failed to extract artist name"))?
        .to_owned();

    let releases = document
        .select(&RELEASES_SELECTOR)
        .take(Configuration::get().bandcamp.releases_limit)
        .map(|element| match element.value().attr("href") {
            Some(v) => {
                trace!(release_url = v, "found release URL");
                if v.starts_with('/') {
                    let url = format!("{}{}", artist_id.0, v);
                    Url::parse(&url)
                } else {
                    Url::parse(v)
                }
                .context("Invalid release URL")
            }
            None => Err(anyhow!("Failed to extract release URL")),
        })
        .collect::<Result<Vec<_>, _>>()?;

    debug!(?name, releases = releases.len());

    Ok(BandcampArtist { name, releases })
}

#[derive(Debug, Deserialize)]
struct BandcampRelease {
    #[serde(rename = "@id")]
    id: BandcampReleaseId,
    name: String,
    #[serde(rename = "byArtist")]
    artist: LinkedDataArtist,
    publisher: LinkedDataPublisher,
    image: String,
    #[serde(rename = "dateModified", deserialize_with = "bandcamp_date")]
    modified: OffsetDateTime,
    #[serde(rename = "datePublished", deserialize_with = "bandcamp_date")]
    published: OffsetDateTime,
}

impl From<BandcampRelease> for Release {
    fn from(r: BandcampRelease) -> Self {
        let artist_url = r.artist.id.unwrap_or(r.publisher.id);

        Release {
            id: ReleaseId::Bandcamp(r.id),
            name: r.name,
            artists: vec![Artist {
                name: r.artist.name,
                id: BandcampArtistId::from_url(&artist_url).into(),
            }],
            release_date: cmp::min(r.modified, r.published),
            cover_image: Some(r.image),
        }
    }
}

#[derive(Debug, Deserialize)]
struct LinkedDataArtist {
    name: String,
    #[serde(rename = "@id")]
    id: Option<Url>,
}

#[derive(Debug, Deserialize)]
struct LinkedDataPublisher {
    #[serde(rename = "@id")]
    id: Url,
}

#[instrument(skip_all)]
async fn get_releases(
    client: &Client,
    last_request: &mut Instant,
    releases: Vec<Url>,
) -> Result<Vec<BandcampRelease>> {
    let mut result = Vec::with_capacity(releases.len());

    for release in releases {
        wait_between_requests(last_request).await;

        let release_span = info_span!("single_release", %release);
        let release = get_single_release(client, release)
            .instrument(release_span)
            .await
            .context("Failed to get release")?;

        result.push(release);
    }

    Ok(result)
}

async fn get_single_release(client: &Client, release: Url) -> Result<BandcampRelease> {
    trace!("loading release");

    let resp = client.get(release).send().await?;

    let status = resp.status();
    let body = resp.text().await?;

    if status != StatusCode::OK {
        error!(code = ?status, "release page returned unexpected status code");
        bail!("Unexpected status code");
    }

    scrape_release_page(&body)
}

fn scrape_release_page(body: &str) -> Result<BandcampRelease> {
    static LINKED_DATA_SELECTOR: LazyLock<Selector> =
        LazyLock::new(|| Selector::parse("script[type=\"application/ld+json\"]").unwrap());

    let document = Html::parse_document(body);

    let linked_data = document
        .select(&LINKED_DATA_SELECTOR)
        .flat_map(|e| e.text())
        .next()
        .ok_or_else(|| anyhow!("Failed to extract linked data element"))?;

    serde_json::from_str(linked_data).context("Failed to deserialize linked data")
}

const BANDCAMP_DATE: &[FormatItem<'static>] =
    format_description!("[day] [month repr:short] [year] [hour]:[minute]:[second] GMT");

fn bandcamp_date<'de, D>(deserializer: D) -> Result<OffsetDateTime, D::Error>
where
    D: Deserializer<'de>,
{
    let raw = <&str>::deserialize(deserializer)?;
    match PrimitiveDateTime::parse(raw, BANDCAMP_DATE) {
        Ok(d) => Ok(d.assume_utc()),
        Err(e) => Err(<D::Error>::custom(e)),
    }
}
