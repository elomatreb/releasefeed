use std::{
    fmt,
    time::{Duration, Instant},
};

use anyhow::{anyhow, Context};
use axum::http::{header, HeaderValue};
use reqwest::{Client, StatusCode};
use serde::{de::DeserializeOwned, Deserialize, Deserializer, Serialize};
use time::{Date, Month, OffsetDateTime};
use tokio::{
    sync::{
        mpsc::{self, Receiver, Sender},
        oneshot,
    },
    time::sleep,
};
use tracing::{debug, error, info_span, instrument, trace, warn, Instrument};

use crate::{
    entities::{Artist, ArtistId, FeedData, Release, ReleaseId},
    providers::RequestError,
    Configuration,
};

const LOGIN_URL: &str = "https://accounts.spotify.com/api/token";

const API_BASE_URL: &str = "https://api.spotify.com/v1";

const DEFAULT_TIMEOUT: Duration = Duration::from_secs(5);

#[derive(Clone)]
pub(crate) struct SpotifyActor {
    tx: Sender<Action>,
}

impl SpotifyActor {
    pub(super) fn start() -> SpotifyActor {
        let span = info_span!("spotify");
        debug!(parent: &span, "starting Spotify actor");

        let (tx, rx) = mpsc::channel(4);

        tokio::spawn(SpotifyActor::run(rx).instrument(span));

        SpotifyActor { tx }
    }

    pub(super) async fn load(&self, artist_id: SpotifyId) -> Result<FeedData, RequestError> {
        let (tx, rx) = oneshot::channel();
        self.send_action(Action::GetReleasesForArtist {
            artist_id: artist_id.clone(),
            responder: tx,
        })
        .await;

        if let Some(releases) = rx.await.expect("actor did not respond")? {
            let artist_name =
                if let Some(n) = artist_name_from_existing(artist_id.clone(), &releases) {
                    n
                } else {
                    let (tx, rx) = oneshot::channel();
                    self.send_action(Action::GetArtistName {
                        artist_id: artist_id.clone(),
                        responder: tx,
                    })
                    .await;

                    rx.await.expect("actor did not respond")?
                };

            Ok(FeedData {
                artist: Artist {
                    id: artist_id.into(),
                    name: artist_name,
                },
                releases: releases.into_iter().map(Release::from).collect(),
                retrieved_at: OffsetDateTime::now_utc(),
            })
        } else {
            Err(RequestError::NotFound)
        }
    }

    async fn send_action(&self, action: Action) {
        self.tx.send(action).await.expect("actor is not running");
    }

    async fn run(mut requests: Receiver<Action>) {
        let client = Client::builder().gzip(true).build().unwrap();

        let mut login_token = String::new();
        let mut login_lifetime = None;
        let mut login_updated = Instant::now();

        debug!("actor running");

        while let Some(action) = requests.recv().await {
            trace!(?action);

            if login_lifetime < Some(login_updated.elapsed()) {
                debug!("login token is expired");
                match get_access_token(&client)
                    .await
                    .context("Failed to get Spotify login token")
                {
                    Ok(login) => {
                        debug!(lifetime = ?login.expires_in, "received new token");
                        login_token = login.access_token;
                        // Subtract a couple of seconds from the token lifetime to avoid racing
                        // conditions when it's about to expire
                        login_lifetime =
                            Some(login.expires_in.saturating_sub(Duration::from_secs(5)));
                        login_updated = Instant::now();
                    }
                    Err(error) => {
                        match action {
                            Action::GetReleasesForArtist { responder, .. } => {
                                let _ = responder.send(Err(error));
                            }
                            Action::GetArtistName { responder, .. } => {
                                let _ = responder.send(Err(error));
                            }
                        }

                        continue;
                    }
                }
            }

            match action {
                Action::GetReleasesForArtist {
                    artist_id,
                    responder,
                } => {
                    let response = get_releases_for_artist(&client, &login_token, artist_id).await;
                    let _ = responder.send(response);
                }
                Action::GetArtistName {
                    artist_id,
                    responder,
                } => {
                    let response = get_artist_name(&client, &login_token, artist_id).await;
                    let _ = responder.send(response);
                }
            }
        }
    }
}

enum Action {
    GetReleasesForArtist {
        artist_id: SpotifyId,
        responder: oneshot::Sender<Result<Option<Vec<SpotifyRelease>>, anyhow::Error>>,
    },
    GetArtistName {
        artist_id: SpotifyId,
        responder: oneshot::Sender<Result<String, anyhow::Error>>,
    },
}

impl fmt::Debug for Action {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Action::GetReleasesForArtist { artist_id, .. } => f
                .debug_struct("GetReleasesForArtist")
                .field("artist_id", artist_id)
                .finish_non_exhaustive(),
            Action::GetArtistName { artist_id, .. } => f
                .debug_struct("GetArtistName")
                .field("artist_id", artist_id)
                .finish_non_exhaustive(),
        }
    }
}

#[derive(Deserialize)]
struct SpotifyLogin {
    access_token: String,
    #[serde(deserialize_with = "duration_from_secs")]
    expires_in: Duration,
}

fn duration_from_secs<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Duration, D::Error> {
    let secs = Deserialize::deserialize(deserializer)?;
    Ok(Duration::from_secs(secs))
}

fn artist_name_from_existing(artist_id: SpotifyId, releases: &[SpotifyRelease]) -> Option<String> {
    for r in releases {
        for release_artist in &r.artists {
            if release_artist.id == artist_id {
                return Some(release_artist.name.clone());
            }
        }
    }

    None
}

async fn get_access_token(client: &Client) -> Result<SpotifyLogin, anyhow::Error> {
    #[derive(Serialize)]
    struct LoginRequest {
        grant_type: &'static str,
    }

    let config = Configuration::get();
    let response = client
        .post(LOGIN_URL)
        .basic_auth(
            &config.spotify.client_id,
            Some(&config.spotify.client_secret),
        )
        .form(&LoginRequest {
            grant_type: "client_credentials",
        })
        .send()
        .await?;

    let response = response.json::<SpotifyLogin>().await?;

    Ok(response)
}

#[instrument(skip(client, login_token))]
async fn get_releases_for_artist(
    client: &Client,
    login_token: &str,
    id: SpotifyId,
) -> Result<Option<Vec<SpotifyRelease>>, anyhow::Error> {
    trace!("loading releases");
    let url = format!("{}/artists/{}/albums", API_BASE_URL, id.0);

    if let Some(r) = get_api::<ArtistsReleases>(client, login_token, &url).await? {
        debug!(count = r.releases.len(), "loaded releases");
        Ok(Some(r.releases))
    } else {
        debug!("artist does not exist");
        Ok(None)
    }
}

async fn get_artist_name(
    client: &Client,
    login_token: &str,
    id: SpotifyId,
) -> Result<String, anyhow::Error> {
    let url = format!("{}/artist/{}", API_BASE_URL, id.0);

    if let Some(a) = get_api::<SpotifyArtist>(client, login_token, &url).await? {
        Ok(a.name)
    } else {
        Err(anyhow!("artist ID not found when getting name"))
    }
}

#[derive(Clone, PartialEq, Eq, Deserialize, Serialize)]
#[serde(try_from = "String")]
pub(crate) struct SpotifyId(String);

impl TryFrom<String> for SpotifyId {
    type Error = &'static str;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if value.len() >= 22 && value.chars().all(|c| c.is_ascii_alphanumeric()) {
            Ok(SpotifyId(value))
        } else {
            Err("invalid Spotify ID")
        }
    }
}

impl fmt::Debug for SpotifyId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Display for SpotifyId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<SpotifyId> for ArtistId {
    fn from(id: SpotifyId) -> Self {
        ArtistId::Spotify(id)
    }
}

impl From<SpotifyId> for ReleaseId {
    fn from(id: SpotifyId) -> Self {
        ReleaseId::Spotify(id)
    }
}

#[derive(Debug, Deserialize)]
struct ArtistsReleases {
    #[serde(rename = "items")]
    releases: Vec<SpotifyRelease>,
}

#[derive(Debug, Deserialize)]
struct SpotifyRelease {
    id: SpotifyId,
    name: String,
    artists: Vec<SpotifyArtist>,
    release_date: String,
    release_date_precision: ReleaseDatePrecision,
    images: Vec<ReleaseImage>,
}

impl SpotifyRelease {
    fn release_date(&self) -> Date {
        let limit = match self.release_date_precision {
            ReleaseDatePrecision::Year => 1,
            ReleaseDatePrecision::Month => 2,
            ReleaseDatePrecision::Day => 3,
        };

        let mut segments = self.release_date.split('-').take(limit);

        let year = segments.next().unwrap().parse().unwrap();
        let month = segments
            .next()
            .map(|v| v.parse::<u8>().unwrap())
            .and_then(|m| Month::try_from(m).ok())
            .unwrap_or(Month::January);
        let day = segments.next().map_or(1, |v| v.parse().unwrap());

        Date::from_calendar_date(year, month, day).unwrap()
    }
}

impl From<SpotifyRelease> for Release {
    fn from(r: SpotifyRelease) -> Self {
        let release_date = r.release_date().midnight().assume_utc();

        Release {
            id: r.id.into(),
            name: r.name,
            artists: r.artists.into_iter().map(Artist::from).collect(),
            release_date,
            cover_image: r.images.into_iter().max_by_key(|i| i.width).map(|i| i.url),
        }
    }
}

#[derive(Debug, Deserialize)]
struct SpotifyArtist {
    id: SpotifyId,
    name: String,
}

impl From<SpotifyArtist> for Artist {
    fn from(a: SpotifyArtist) -> Self {
        Artist {
            id: a.id.into(),
            name: a.name,
        }
    }
}

#[derive(Debug, Deserialize)]
struct ReleaseImage {
    url: String,
    width: u64,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
enum ReleaseDatePrecision {
    Year,
    Month,
    Day,
}

#[instrument(skip(client, login_token))]
async fn get_api<T>(
    client: &Client,
    login_token: &str,
    url: &str,
) -> Result<Option<T>, anyhow::Error>
where
    T: DeserializeOwned,
{
    trace!("sending request");

    loop {
        let resp = client.get(url).bearer_auth(login_token).send().await?;

        let status = resp.status();
        let retry_after = retry_after(resp.headers().get(header::RETRY_AFTER));
        let body = resp.bytes().await?;

        match status {
            StatusCode::OK => {
                trace!("completed request successfully");
                let data = serde_json::from_slice(&body)?;
                break Ok(Some(data));
            }
            StatusCode::NOT_FOUND => {
                trace!("request returned 404");
                break Ok(None);
            }
            StatusCode::TOO_MANY_REQUESTS => {
                let wait = retry_after.unwrap_or(DEFAULT_TIMEOUT);
                warn!(?wait, "hit API rate limit");
                sleep(wait).await;
                trace!("wait complete, retrying request");
            }
            other => {
                error!(code = ?other, "API returned unexpected status code");
                break Err(anyhow!("unexpected status code"));
            }
        }
    }
}

fn retry_after(header: Option<&HeaderValue>) -> Option<Duration> {
    header.and_then(|h| {
        let seconds = h
            .to_str()
            .ok()
            .and_then(|v| v.parse::<u64>().ok())
            .map(Duration::from_secs);

        if seconds.is_none() {
            warn!(header = ?h, "invalid Retry-After header");
        }

        seconds
    })
}
